<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\User;
use Google_Client;

// require_once 'vendor/autoload.php';


class UserController extends Controller
{
    //
   public function getToken(Request $request)
   {
     $username = $request->input('username');
     $password = $request->input('password');

    $client = new Client([
      'base_uri' => 'http://localhost/matchinger/public/',
      'timeout'  => 5.0,
      'defaults' => [
        'exceptions' => false
      ]
    ]);

    $response = $client->request('POST', 'oauth/token', [
        'form_params' => [
              'grant_type' => 'password',
              'client_id' => '2',
              'client_secret' => 'bnYLtyd1POFQNz1taO7CMg90lOnMr2fqM2K99JQR',
              'username' => $username,
              'password' => $password,
              'scope' => ''
        ]
    ]);
    return json_decode((string) $response->getBody(), true);
   }

   public function getTokenByGoogle(Request $request)
   {
     $id_token = $request->input('googleToken');
     $name = $request->input('name');
     $client = new Google_Client(['client_id' => '9524139961-9op4hdohoghe7ehrgaasc2sun3qs5tnt.apps.googleusercontent.com']);  // Specify the CLIENT_ID of the app that accesses the backend
      $payload = $client->verifyIdToken($id_token);
      if ($payload) {
        $xId = $payload['sub'];
        // If request specified a G Suite domain:
        //$domain = $payload['hd'];
       $token = $this->getTokenByExternal('google',$xId, $name);
       return response()->json(['token' => $token], 200);
      } else {
        // Invalid ID token
        App::abort(401, 'Not authenticated');
      }
   }

   private function getTokenByExternal($xType, $xId, $name)
   {
    //  $user = User::where(['xoauthType' => 'google', 'xoauthID' => $googleToken])
    //            // ->orderBy('name', 'desc')
    //            // ->take(10)
    //            ->get();
    // echo $user;
    // // create user if no existing user for this google auth
    // if(!$user){
    //
    // }
      $user = User::firstOrCreate(
        ['xoauthType' => $xType, 'xoauthID' => $xId],
        ['name' => $name]
      );
      $token = $user->createToken($xType.'-token')->accessToken;
      // echo $token;
      return $token;
   }
}
