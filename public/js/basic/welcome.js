$(function(){

})

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail());
  var id_token = googleUser.getAuthResponse().id_token; // This is null if the 'email' scope is not present.
  var name = profile.getName();
  $.ajax({
    url: '/matchinger/public/api/get-token-by-google',
    dataType: 'json',
    type: 'POST',
    data: {googleToken: id_token, name: name},
    success: function (response, status) {
        console.log('AJAX success: ' + response);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        console.log('AJAX error:' + textStatus);
    }
});
}
